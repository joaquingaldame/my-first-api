package com.folcademy.MyFirstApi.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Goodbye {
    @PostMapping ("/goodbye")
    public String goodbye(){
        return "Goodbye World!!!";
    }
}
